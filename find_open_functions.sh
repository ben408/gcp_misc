#!/bin/bash

#############################
# Run this tool to find Cloud Functions that permit unauthenticated invocations
# anywhere in your GCP organization.
# Enjoy!
#############################

for proj in $(gcloud projects list --format="get(projectId)"); do
    echo "[*] scraping project $proj"

    enabled=$(gcloud services list --project "$proj" | grep "Cloud Functions API")

    if [ -z "$enabled" ]; then
	continue
    fi


    for func in $(gcloud functions list --quiet --project $proj | tail -n +2 | awk '{print $1}'); do
        ACL="$(gcloud functions get-iam-policy $func --project $proj)"

        all_users="$(echo $ACL | grep allUsers)"
        all_auth="$(echo $ACL | grep allAuthenticatedUsers)"

        if [ -z "$all_users" ]
        then
              :
        else
              echo "[!] Open to all users: $proj: $func"
        fi

        if [ -z "$all_auth" ]
        then
              :
        else
              echo "[!] Open to all authenticated users: $proj: $func"
        fi
    done
done
